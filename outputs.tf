output "private_ip" {
  description = "List of private IP addresses assigned to the instances"
  value       = aws_instance.ec2.*.private_ip
}

output "tags" {
  description = "List of tags of instances"
  value       = aws_instance.ec2.*.tags
}
